<?php

function landportal_install_tasks_alter(&$tasks, $install_state) {
    // install_state: install_profile_modules
    // remove module from minimal install profile
    /* module_uninstall('overlay', FALSE); */
    /* module_uninstall('toolbar', FALSE); */
    //var_dump($tasks);
    error_log('removed minimal modules');
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function landportal_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}
