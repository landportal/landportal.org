<?php
/**
 * @file
 * test_exportar_webform.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function test_exportar_webform_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['webform-client-block-89545'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'client-block-89545',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'webform',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lpbs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'lpbs',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
